from __future__ import annotations
from decimal import Decimal
from . import constants
from django.db import models, transaction
from django.db.models import Q
from django.db.models.query import QuerySet
from django.contrib.auth.models import User
from .errors import InsufficientFunds
import uuid


class Rank(models.Model):
    name = models.CharField(max_length=15, unique=True, db_index=True)
    value = models.IntegerField(unique=True, db_index=True)

    @classmethod
    def default_rank(cls) -> Rank:
        return cls.objects.all().aggregate(models.Min('value'))['value__min']

    def __str__(self):
        return f'{self.value}:{self.name}'


class Customer(models.Model):
    user = models.OneToOneField(
        User, primary_key=True, on_delete=models.PROTECT)
    phone = models.CharField(max_length=15)
    rank = models.ForeignKey(Rank, default=2, on_delete=models.PROTECT)
    designated_employee = models.ForeignKey(
        User, on_delete=models.PROTECT, related_name='+', limit_choices_to={'is_staff': True})
    # User.objects.filter(is_staff=True).first() -- query to get all Staff

    @property
    def full_name(self) -> str:
        return f'{self.user.first_name} {self.user.last_name}'

    @property
    def personal_id(self) -> str:
        return self.pk

    @property
    def accounts(self) -> QuerySet:
        return Account.objects.filter(user=self.user)

    @property
    def can_make_loan(self) -> bool:
        return self.rank.value >= constants.CUSTOMER_RANK_LOAN

    @property
    def default_account(self) -> Account:
        return Account.objects.filter(user=self.user).first()

    def make_loan(self, amount, name):
        assert self.can_make_loan, 'User rank does not allow for making loans.'
        assert amount >= 0, 'Negative amount not allowed for loan.'
        loan_account = Account.objects.create(
            user=self.user, name=f'Loan: {name}')
        #loan_account_pk = loan.pk
        print(loan_account, '<<-- loan_account_pk')
        #payout_account_pk = self.default_account.pk
        payout_account = self.default_account
        Ledger.transfer(
            amount,
            loan_account,
            f'Loan paid out to account {self.default_account}',
            payout_account,
            f'Credit from loan {loan_account.pk}: {loan_account.name}',
            is_loan=True
        )

    @classmethod
    def search(cls, search_term):
        return cls.objects.filter(
            Q(user__username__contains=search_term) |
            Q(user__first_name__contains=search_term) |
            Q(user__last_name__contains=search_term) |
            Q(user__email__contains=search_term) |
            # Q(personal_id__contains=search_term)      |
            Q(phone__contains=search_term)
        )[:15]

    def __str__(self):
        return f'Employee: {self.designated_employee} -- {self.personal_id}: {self.full_name}'


class Bank(models.Model):
    bank_name = models.CharField(max_length=30)
    bank_id = models.CharField(max_length=4, unique=True)
    country_code = models.CharField(max_length=2)

    @classmethod
    def get_default_bank(cls):
        return cls.objects.get(bank_id=constants.BANK_ID)

    def __str__(self):
        return f'Bank: {self.bank_name}, ID: {self.bank_id}, Country: {self.country_code}'


class Account(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    name = models.CharField(max_length=50, db_index=True)
    # bank_id = models.ForeignKey(Bank, on_delete=models.PROTECT, default=Bank.get_default_bank)

    class Meta:
        get_latest_by = 'pk'

    @property
    def movements(self) -> QuerySet:
        # TODO: Add is_succeeded = true
        # Bank.objects.get(bank_id=BANK_ID)
        return Ledger.objects.filter(account_number=self.pk, bank_id=Bank.objects.get(bank_id=constants.BANK_ID))

    @property
    def balance(self) -> Decimal:
        return self.movements.aggregate(models.Sum('amount'))['amount__sum'] or Decimal(0)

    def __str__(self):
        return f'{self.pk} :: {self.user} :: {self.name}'


class Ledger(models.Model):
    account_number = models.CharField(max_length=10)
    transaction_id = models.CharField(max_length=36)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add=True, db_index=True)
    transfer_text = models.TextField()
    bank_id = models.ForeignKey(
        Bank, on_delete=models.PROTECT, default=Bank.get_default_bank)
    is_transaction_succeded = models.BooleanField(default=False)

    @classmethod
    def transfer(cls, amount, debit_account, debit_text, credit_account, credit_text, is_loan=False,) -> str:
        assert amount >= 0, 'Negative amount not allowed for transfer.'
        print('#### transfer <4> #####')

        with transaction.atomic():
            if debit_account.balance >= amount or is_loan:
                # Generate transaction_id
                uid = uuid.uuid4()
                # Get the account numbers
                debit_account_number = debit_account.pk
                credit_account_number = credit_account.pk
                print('Just before Ledger Write -> ',
                      debit_account_number, credit_account_number)
                # Update ledger - debit and credit entry
                cls(amount=-amount, transaction_id=uid, account_number=debit_account_number,
                    transfer_text=debit_text, is_transaction_succeded=True).save()
                cls(amount=amount, transaction_id=uid, account_number=credit_account_number,
                    transfer_text=credit_text, is_transaction_succeded=True).save()
            else:
                raise InsufficientFunds
        return uid

    @classmethod
    def external_transfer(cls, amount, debit_account_bank_id, credit_account_bank_id, debit_account_number, credit_account_number, debit_text, credit_text, transaction_id='', is_transaction_succeded=False) -> str:
        assert amount >= 0, 'Negative amount not allowed for transfer.'
        print('#### transfer <4> #####')
        print(type(amount), amount, debit_account_bank_id, credit_account_bank_id,
              debit_account_number, credit_account_number, debit_text, credit_text)
        print(transaction_id, '<<-- Transaction id inside external_transfer')

        debit_account_bank_instance = Bank.objects.get(
            bank_id=debit_account_bank_id)
        credit_account_bank_instance = Bank.objects.get(
            bank_id=credit_account_bank_id)

        credit_account_bank_id = credit_account_bank_instance
        debit_account_bank_id = debit_account_bank_instance
        # Receiving money
        if not transaction_id:
            # receiving money
            print(transaction_id, '<<-- Transaction id (if not)')
            with transaction.atomic():
                # Generate transaction_id
                uid = uuid.uuid4()
                # Get the account numbers
                # Update ledger - debit and credit entry
                cls(amount=-amount, transaction_id=uid, account_number=debit_account_number,
                    transfer_text=debit_text, bank_id=debit_account_bank_id).save()
                cls(amount=amount, transaction_id=uid, account_number=credit_account_number,
                    transfer_text=credit_text, bank_id=credit_account_bank_id).save()
                print(uid)
            return uid
        else:
            print(transaction_id, '<<-- Transaction id (else)')
            with transaction.atomic():
                # receiving transaction id
                cls(amount=-amount, account_number=debit_account_number, transfer_text=debit_text,
                    bank_id=debit_account_bank_id, transaction_id=transaction_id, is_transaction_succeded=True).save()
                cls(amount=amount, account_number=credit_account_number, transfer_text=credit_text,
                    bank_id=credit_account_bank_id, transaction_id=transaction_id, is_transaction_succeded=True).save()
                return transaction_id

    @staticmethod
    def balance() -> Decimal:
        return Ledger.objects.all().aggregate(models.Sum('amount'))['amount__sum']

    def __str__(self):
        if self.amount > 0:
            return f'ID: {self.transaction_id}, Credit Account: {self.account_number}, Amount: {self.amount}'
        else:
            return f'ID: {self.transaction_id}, Debit Account: {self.account_number}, Amount: {self.amount}'
    # def __str__(self):
    #     return f'Status: {self.is_transaction_succeded} :: Amount: {self.amount} :: {self.transaction_id} :: Bank: {self.bank_id} :: Account: {self.account_number} :: {self.transfer_text}'
