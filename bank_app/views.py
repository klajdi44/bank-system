from decimal import Decimal
from secrets import token_urlsafe
# CSRF TOKEN IGNORE
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, JsonResponse
from django.http import QueryDict
from django.shortcuts import render, reverse, get_object_or_404
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.db import IntegrityError
from .forms import TransferForm, UserForm, CustomerForm, NewUserForm, NewAccountForm, ExternalTransferForm
from .models import Account, Ledger, Customer
from .errors import InsufficientFunds
from django.core.exceptions import ObjectDoesNotExist
from .constants import BANK_ID
import requests
import uuid


@login_required
def index(request):
    if request.user.is_staff:
        return HttpResponseRedirect(reverse('bank_app:staff_dashboard'))
    else:
        return HttpResponseRedirect(reverse('bank_app:dashboard'))


# Customer views
@login_required(redirect_field_name=None)
def dashboard(request):
    assert not request.user.is_staff, 'Staff user routing customer view.'

    accounts = request.user.customer.accounts
    context = {
        'accounts': accounts,
    }
    return render(request, 'bank_app/dashboard.html', context)


@login_required
def account_details(request, pk):
    assert not request.user.is_staff, 'Staff user routing customer view.'

    account = get_object_or_404(Account, user=request.user, pk=pk)
    print(account, '<<<-----------')
    context = {
        'account': account,
    }
    return render(request, 'bank_app/account_details.html', context)


@login_required
def transaction_details(request, transaction):
    movements = Ledger.objects.filter(transaction_id=transaction)
    if not request.user.is_staff:
        print('Good')
        # Updated account__in to --> account_number__in
        if not movements.filter(account_number__in=request.user.customer.accounts):
            print('Bad')
            raise PermissionDenied('Customer is not part of the transaction.')
    context = {
        'movements': movements,
    }
    return render(request, 'bank_app/transaction_details.html', context)


@login_required
def make_transfer(request):
    assert not request.user.is_staff, 'Staff user routing customer view.'

    if request.method == 'POST':
        transfer_form = TransferForm(request.POST)
        transfer_form.fields['debit_account'].queryset = request.user.customer.accounts

        if transfer_form.is_valid():

            amount = transfer_form.cleaned_data['amount']
            debit_account = Account.objects.get(
                pk=transfer_form.cleaned_data['debit_account'].pk)
            debit_text = transfer_form.cleaned_data['debit_text']
            credit_account = Account.objects.get(
                pk=transfer_form.cleaned_data['credit_account'])
            credit_text = transfer_form.cleaned_data['credit_text']

            # Check if the account transfer is between same account:
            if debit_account == credit_account:
                print('Transfering to same account : debit_account == credit_account')
                context = {
                    'title': 'Transfer Error',
                    'error': 'The receiving account must be different from the sender account.'
                }
                return render(request, 'bank_app/error.html', context)

            try:
                transfer = Ledger.transfer(
                    amount, debit_account, debit_text, credit_account, credit_text)
                return transaction_details(request, transfer)
            except InsufficientFunds:
                context = {
                    'title': 'Transfer Error',
                    'error': 'Insufficient funds for transfer.'
                }
                return render(request, 'bank_app/error.html', context)
    else:
        transfer_form = TransferForm()
    transfer_form.fields['debit_account'].queryset = request.user.customer.accounts
    context = {
        'transfer_form': transfer_form,
    }
    return render(request, 'bank_app/make_transfer.html', context)


@csrf_exempt
@login_required
def make_external_transfer(request):
    form = ExternalTransferForm(request.POST or None)
    form.fields['debit_account'].queryset = request.user.customer.accounts
    if form.is_valid():
        # Store data as a dictionary
        data = request.POST.dict()
        # Add debit_account_bank to data
        data['debit_account_bank'] = BANK_ID

        # Remove csrf token from data
        data.pop('csrfmiddlewaretoken')

        # Try connecting to the receiving bank
        try:
            r = requests.post(
                f'http://localhost:8000/receive_external_transfer/', data=data, timeout=10)
            if r.status_code == requests.codes.ok:
                r = r.json()
                amount = int(data['amount'])
                debit_text = data['debit_text']
                credit_text = data['credit_text']
                # credit account number of the receiver
                credit_account_number = data['credit_account']
                # debit account number of the receiver
                debit_account_number = data['debit_account']
                # ID of the sending
                debit_account_bank_id = data['debit_account_bank']
                # ID of the receiving bank
                credit_account_bank_id = data['credit_account_bank']
                try:
                    transaction_id = r['transaction_id']
                    put_data = {
                        'transaction_id': transaction_id,
                    }
                    # write to ledger
                    Ledger.external_transfer(amount=amount, debit_account_bank_id=debit_account_bank_id, credit_account_bank_id=credit_account_bank_id,
                                             debit_account_number=debit_account_number,
                                             credit_account_number=credit_account_number,
                                             debit_text=debit_text, credit_text=credit_text, transaction_id=transaction_id, is_transaction_succeded=True)

                    try:
                        # after success
                        r = requests.put(
                            f'http://localhost:8000/receive_external_transfer/', data=put_data, timeout=10)
                        if r.status_code == requests.codes.ok:
                            return transaction_details(request, transaction_id)
                    except:
                        context['title'] = 'Transfer Error'
                        context['error'] = 'Receiving bank not responding.'

                except:
                    r = requests.delete(
                        f'http://localhost:8000/receive_external_transfer/', data=put_data, timeout=10)

        except:
            # If request fails
            context = {
                'title': 'Transfer Error',
                'error': 'Receiving bank not responding.'
            }
            return render(request, 'bank_app/error.html', context)

    else:
        print('###### External: Invalid - try again!')

    context = {
        'form': form
    }
    return render(request, 'bank_app/make_external_transfer.html', context)


@login_required
def make_loan(request):
    assert not request.user.is_staff, 'Staff user routing customer view.'

    if not request.user.customer.can_make_loan:
        context = {
            'title': 'Create Loan Error',
            'error': 'Loan could not be completed.'
        }
        return render(request, 'bank_app/error.html', context)
    if request.method == 'POST':
        request.user.customer.make_loan(
            Decimal(request.POST['amount']), request.POST['name'])
        return HttpResponseRedirect(reverse('bank_app:dashboard'))
    return render(request, 'bank_app/make_loan.html', {})

# Staff views


@login_required
def staff_dashboard(request):
    assert request.user.is_staff, 'Customer user routing staff view.'

    return render(request, 'bank_app/staff_dashboard.html')


@login_required
def staff_search_partial(request):
    assert request.user.is_staff, 'Customer user routing staff view.'

    search_term = request.POST['search_term']
    customers = Customer.search(search_term)
    context = {
        'customers': customers,
    }
    return render(request, 'bank_app/staff_search_partial.html', context)


@login_required
def staff_customer_details(request, pk):
    assert request.user.is_staff, 'Customer user routing staff view.'

    customer = get_object_or_404(Customer, pk=pk)
    if request.method == 'GET':
        user_form = UserForm(instance=customer.user)
        customer_form = CustomerForm(instance=customer)
    elif request.method == 'POST':
        user_form = UserForm(request.POST, instance=customer.user)
        customer_form = CustomerForm(request.POST, instance=customer)
        if user_form.is_valid() and customer_form.is_valid():
            user_form.save()
            customer_form.save()
    new_account_form = NewAccountForm()
    context = {
        'customer': customer,
        'user_form': user_form,
        'customer_form': customer_form,
        'new_account_form': new_account_form,
    }
    return render(request, 'bank_app/staff_customer_details.html', context)


@login_required
def staff_account_list_partial(request, pk):
    assert request.user.is_staff, 'Customer user routing staff view.'

    customer = get_object_or_404(Customer, pk=pk)
    accounts = customer.accounts
    context = {
        'accounts': accounts,
    }
    return render(request, 'bank_app/staff_account_list_partial.html', context)


@login_required
def staff_account_details(request, pk):
    assert request.user.is_staff, 'Customer user routing staff view.'

    account = get_object_or_404(Account, pk=pk)
    context = {
        'account': account,
    }
    return render(request, 'bank_app/account_details.html', context)


@login_required
def staff_new_account_partial(request, user):
    assert request.user.is_staff, 'Customer user routing staff view.'

    if request.method == 'POST':
        new_account_form = NewAccountForm(request.POST)
        if new_account_form.is_valid():
            Account.objects.create(user=User.objects.get(
                pk=user), name=new_account_form.cleaned_data['name'])
    return HttpResponseRedirect(reverse('bank_app:staff_customer_details', args=(user,)))


@login_required
def staff_new_customer(request):
    assert request.user.is_staff, 'Customer user routing staff view.'

    if request.method == 'POST':
        new_user_form = NewUserForm(request.POST)
        customer_form = CustomerForm(request.POST)
        if new_user_form.is_valid() and customer_form.is_valid():
            username = new_user_form.cleaned_data['username']
            first_name = new_user_form.cleaned_data['first_name']
            last_name = new_user_form.cleaned_data['last_name']
            email = new_user_form.cleaned_data['email']
            password = new_user_form.cleaned_data['password']
            rank = customer_form.cleaned_data['rank']
            # personal_id = customer_form.cleaned_data['personal_id']
            phone = customer_form.cleaned_data['phone']
            designated_employee = User.objects.get(
                pk=customer_form.cleaned_data['designated_employee'].first().pk)
            print('**********', designated_employee, rank)
            try:
                user = User.objects.create_user(
                    username=username,
                    password=password,
                    email=email,
                    first_name=first_name,
                    last_name=last_name
                )
                print(
                    f'********** Username: {username} -- Password: {password}')
                Customer.objects.create(
                    user=user, rank=rank, phone=phone, designated_employee=designated_employee)
                return staff_customer_details(request, user.pk)
            except IntegrityError:
                context = {
                    'title': 'Database Error',
                    'error': 'User could not be created.'
                }
                return render(request, 'bank_app/error.html', context)
    else:
        new_user_form = NewUserForm()
        customer_form = CustomerForm()
    context = {
        'new_user_form': new_user_form,
        'customer_form': customer_form,
    }
    return render(request, 'bank_app/staff_new_customer.html', context)


# For Transactions between banks
@csrf_exempt
def receive_external_transfer(request):

    if request.method == 'POST':
        data = request.POST.dict()
        amount = int(data['amount'])
        debit_text = data['debit_text']
        credit_text = data['credit_text']
        # credit account number of the receiver
        credit_account_number = data['credit_account']
        # debit account number of the receiver
        debit_account_number = data['debit_account']
        # ID of the sending
        debit_account_bank_id = data['debit_account_bank']
        # ID of the receiving bank
        credit_account_bank_id = data['credit_account_bank']
        transactionId = ''

        try:
            # Check if receiving account exists
            recieverAccount = Account.objects.get(pk=credit_account_number)
        except ObjectDoesNotExist:
            return HttpResponseBadRequest

        try:
            transactionId = Ledger.external_transfer(amount=amount, debit_account_bank_id=debit_account_bank_id, credit_account_bank_id=credit_account_bank_id,
                                                     debit_account_number=debit_account_number,
                                                     credit_account_number=credit_account_number,
                                                     debit_text=debit_text, credit_text=credit_text)
        except:
            print(f'******************** error')
            pass
        print(transactionId)
        return JsonResponse({'transaction_id': transactionId})

    if request.method == 'PUT':
        put = QueryDict(request.body)
        transaction_id = put.get('transaction_id')
        Ledger.objects.filter(transaction_id=transaction_id).update(
            is_transaction_succeded=True)
        print('******* success')

    if request.method == 'DELETE':
        print('<<<---- Receiving a DELETE request')
        delete = QueryDict(request.body)
        transaction_id = delete.get('transaction_id')
        Ledger.objects.filter(transaction_id=transaction_id).delete()
        print('******* success')

    return HttpResponse('<h1>Receive External Transfer</h1>')
