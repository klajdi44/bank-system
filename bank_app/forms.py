from django.conf import settings
from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from .models import Customer, Account, Ledger, Bank
from .constants import BANK_ID


class TransferForm(forms.Form):
    amount = forms.DecimalField(label='Amount', max_digits=10, widget=forms.NumberInput(
        attrs={
            'class': 'form-control',
        }
    ))
    debit_account = forms.ModelChoiceField(label='Debit Account', queryset=Customer.objects.none(), widget=forms.Select(
        attrs={
            'class': 'form-control',
        }
    ))
    debit_text = forms.CharField(label='Debit Account Text', max_length=25,  widget=forms.TextInput(
        attrs={
            'class': 'form-control',
        }
    ))
    credit_account = forms.IntegerField(label='Credit Account Number',  widget=forms.NumberInput(
        attrs={
            'class': 'form-control',
        }
    ))
    credit_text = forms.CharField(label='Credit Account Text', max_length=25,  widget=forms.TextInput(
        attrs={
            'class': 'form-control',
        }
    ))

    class Meta:
        fields = ('amount', 'debit_account')
        widgets = {
            'amount': forms.NumberInput(attrs={'class': 'form-control'}),
        }

    def clean(self):
        super().clean()

        # Ensure credit account exist
        credit_account = self.cleaned_data.get('credit_account')
        try:
            Account.objects.get(pk=credit_account)
        except ObjectDoesNotExist:
            self._errors['credit_account'] = self.error_class(
                ['Credit account does not exist.'])

        # Ensure positive amount
        if self.cleaned_data.get('amount') < 0:
            self._errors['amount'] = self.error_class(
                ['Amount must be positive.'])

        return self.cleaned_data


class NewUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password')
        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control'}),
            'first_name': forms.TextInput(attrs={'class': 'form-control'},),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'password': forms.PasswordInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
        }
        error_me = {
            'required': 'Group to which this message belongs to',
        }

    def clean(self):
        super().clean()
        username = self.cleaned_data.get('username')
        if User.objects.filter(username=username):
            self._errors['username'] = self.error_class(
                ['Username already exists.'])
        return self.cleaned_data


class UserForm(forms.ModelForm):
    username = forms.CharField(label='Username', disabled=True)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')
        widgets = {
            'username': forms.TextInput(attrs={'class': 'form-control'}),

            'first_name': forms.TextInput(attrs={'class': 'form-control'}),

            'last_name': forms.TextInput(attrs={'class': 'form-control'}),

            'email': forms.EmailInput(attrs={'class': 'form-control'}),
        }


class CustomerForm(forms.ModelForm):
    designated_employee = forms.ModelMultipleChoiceField(
        queryset=User.objects.filter(is_staff=True))

    class Meta:
        model = Customer
        fields = ('phone', 'rank')
        widgets = {
            'phone': forms.TextInput(attrs={'class': 'form-control'}),
            'rank': forms.Select(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
        }


class NewAccountForm(forms.ModelForm):
    class Meta:
        model = Account
        fields = ('name',)
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
        }


class ExternalTransferForm(forms.Form):
    amount = forms.DecimalField(label='Amount', widget=forms.NumberInput(
        attrs={
            'class': 'form-control',
        }
    ))
    debit_account = forms.ModelChoiceField(queryset=Account.objects.all(), to_field_name='pk', widget=forms.Select(
        attrs={
            'class': 'form-control',
        }
    ))
    debit_text = forms.CharField(label='Text on your account', widget=forms.TextInput(
        attrs={
            'class': 'form-control',
        }
    ))
    credit_account_bank = forms.ModelChoiceField(queryset=Bank.objects.all(), to_field_name='bank_id', label='Receiving bank', widget=forms.Select(
        attrs={
            'class': 'form-control',
        }
    ))
    credit_account = forms.CharField(label='Receivers account number', widget=forms.TextInput(
        attrs={
            'class': 'form-control',
        }
    ))
    credit_text = forms.CharField(label='Text on receivers account', widget=forms.TextInput(
        attrs={
            'class': 'form-control',
        }
    ))

    def clean(self):
        super().clean()

        debit_account_pk = self.cleaned_data.get('debit_account').pk

        # Get debit account
        try:
            debit_account = Account.objects.get(pk=debit_account_pk)
        except ObjectDoesNotExist:
            self._errors['debit_account'] = self.error_class(
                ['Debit account does not exist.'])

        # Ensure that debit account balance is gte amount
        if debit_account.balance <= self.cleaned_data.get('amount'):
            self._errors['debit_account'] = self.error_class(
                ['Insufficient account balance'])

        # Ensure positive amount
        if self.cleaned_data.get('amount') < 0:
            self._errors['amount'] = self.error_class(
                ['Amount must be positive.'])

        return self.cleaned_data
