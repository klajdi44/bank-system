from django.contrib import admin
from django.contrib.admin. sites import AlreadyRegistered
from django.apps import apps

app_config = apps.get_app_config('bank_app') 
models = app_config.get_models()

for model in models:
    try:
        admin.site.register(model)
    except admin.sites.AlreadyRegistered:
        pass