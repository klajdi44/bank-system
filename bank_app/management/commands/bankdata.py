import secrets
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from bank_app.models import Account, Ledger, Customer


class Command(BaseCommand):
    def handle(self, **options):
        print('Adding bank data')

        # Create bank user
        bank_user = User.objects.create_user(
            'bank_user', email='', password=secrets.token_urlsafe(64))
        bank_user.is_active = False
        bank_user.save()
        ipo_account = Account.objects.create(
            user=bank_user, name='Bank IPO Account')
        ops_account = Account.objects.create(
            user=bank_user, name='Bank OPS Account')
        # Adding money
        Ledger.transfer(
            10_000_000,
            ipo_account,
            'Operational Credit',
            ops_account,
            'Operational Credit',
            is_loan=True
        )

        # Create a bank customer one
        bank_a_user = User.objects.create_user(
            'django_bank', email='django@bank.com', password='requirements')
        bank_a_user.first_name = 'Django'
        bank_a_user.last_name = 'Mac'
        bank_a_user.save()
        bank_a_customer = Customer(
            user=bank_a_user, phone='12341234', designated_employee=User.objects.filter(is_staff=True).first())
        bank_a_customer.save()

        # Create account for bank customer one
        bank_a_account = Account.objects.create(
            user=bank_a_user, name='Bank A user account')
        bank_a_account.save()

        # Create a bank customer two
        bank_b_user = User.objects.create_user(
            'test', email='pip@bank.com', password='requirements')
        bank_b_user.first_name = 'Pip'
        bank_b_user.last_name = 'Freeze'
        bank_b_user.save()
        bank_b_customer = Customer(user=bank_b_user, phone='101010',
                                   designated_employee=User.objects.filter(is_staff=True).first())
        bank_b_customer.save()
        # Create account for bank customer two
        bank_b_account = Account.objects.create(
            user=bank_b_user, name='Bank B user account')
        bank_b_account.save()

        # Bank customer two money from bank
        Ledger.transfer(
            10_000,
            ops_account,
            'Payout to Django',
            bank_a_account,
            'Payout from bank',
            is_loan=True
        )

        # Bank customer two money from bank
        Ledger.transfer(
            1_000,
            ops_account,
            'Payout to Pip',
            bank_b_account,
            'Payout from bank'
        )

        print('bankdata done')
