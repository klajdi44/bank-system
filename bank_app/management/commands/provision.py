from django.core.management.base import BaseCommand
from bank_app.models import Rank, Bank
from django.contrib.auth.models import User


class Command(BaseCommand):
    def handle(self, **options):
        print('Provisioning ...')
        if not Rank.objects.all():
            print('Adding ranks')
            Rank.objects.create(name='Platinum', value=60)
            Rank.objects.create(name='Gold', value=50)
            Rank.objects.create(name='Silver', value=30)
            Rank.objects.create(name='Bronze', value=5)

        if not Bank.objects.all():
            print('Adding banks')
            Bank.objects.create(bank_name='Primary Bank',
                                bank_id='2200', country_code='dk')
            Bank.objects.create(bank_name='Py Bank',
                                bank_id='1000', country_code='dk')
            Bank.objects.create(bank_name='Django Bank',
                                bank_id='2000', country_code='dk')
            Bank.objects.create(bank_name='Java Bank',
                                bank_id='3000', country_code='dk')
            Bank.objects.create(bank_name='Shell Bank',
                                bank_id='4000', country_code='dk')
            Bank.objects.create(bank_name='Docker Bank',
                                bank_id='5000', country_code='dk')

        if not User.objects.filter(is_staff=True):
            print('Adding staff users')
            User.objects.create_user(username="Johny", first_name="John", last_name="Dep",
                                     email='', password='bankstaff', is_staff=True).save()
            User.objects.create_user(username="John", first_name="John", last_name="Cena",
                                     email='', password='bankstaff', is_staff=True).save()
            User.objects.create_user(username="Jonas", first_name="Jonas", last_name="Bøgh",
                                     email='', password='bankstaff', is_staff=True).save()
            User.objects.create_user(username="Andrei", first_name="Stefan", last_name="Mihutoni",
                                     email='', password='bankstaff', is_staff=True).save()
            User.objects.create_user(username="Klajdi", first_name="Klajdi", last_name="Ajdini",
                                     email='', password='bankstaff', is_staff=True).save()
