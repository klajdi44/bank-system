import secrets
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from bank_app.models import Account, Ledger, Customer

class Command(BaseCommand):
    def handle(self, **options):
        print('Bank A Data ...')

        # Create bank user
        bank_user = User.objects.create_user('transfer_user', email='', password=secrets.token_urlsafe(64))
        bank_user.is_active = False
        bank_user.save()
        ipo_account = Account.objects.create(user=bank_user, name='Transfer 1')
        ipo_account_number = ipo_account.pk
        ops_account = Account.objects.create(user=bank_user, name='Transfer 2')
        ops_account_number = ops_account.pk
        Ledger.transfer(
            10_000_000,
            ipo_account_number,
            'Operational Credit',
            ops_account_number,
            'Operational Credit',
            is_loan=True
        )


        # Create a Bank A customer
        bank_a_user = User.objects.create_user('kea wd', email='kea@bank.com', password='requirements')
        bank_a_user.first_name = 'Kea'
        bank_a_user.last_name  = 'Lygten'
        bank_a_user.save()
        bank_a_customer = Customer(user=bank_a_user, phone='121212')
        bank_a_customer.save()
        # Create a Bank A account
        bank_a_account = Account.objects.create(user=bank_a_user, name='Kea user account')
        bank_a_account.save()
        bank_a_account_number = bank_a_account.pk

        # Create a Bank B customer
        bank_b_user = User.objects.create_user('Guldbaren', email='guld@bank.com', password='requirements')
        bank_b_user.first_name = 'Guld'
        bank_b_user.last_name  = 'Berg'
        bank_b_user.save()
        bank_b_customer = Customer(user=bank_b_user, phone='101010')
        bank_b_customer.save()
        # Create a Bank B account
        bank_b_account = Account.objects.create(user=bank_b_user, name='Guldberg user account')
        bank_b_account.save()
        bank_b_account_number = bank_b_account.pk


        # Bank A customer money from bank
        Ledger.transfer(
            1_000,
            ops_account_number,
            'Payout to Django',
            bank_a_account_number,
            'Payout from bank',
            is_loan=True
        )

        Ledger.transfer(
            10_000,
            ops_account_number,
            'Payout to Pip',
            bank_b_account_number,
            'Payout from bank',
            is_loan=True
        )
