from django.core.management.base import BaseCommand
from atm_map.models import Atm
 

class Command(BaseCommand):
    def handle(self, **options):
        print('Provisioning atm ...')
        if not Atm.objects.all():
            Atm.objects.create(atm_address='Guldbergsgade 29N, 2200 København',atm_latitude='55.691509',atm_longitude="12.555130")
            Atm.objects.create(atm_address='Øster Voldgade 4A, 1350 København',atm_latitude='55.686199',atm_longitude="12.576480")
            Atm.objects.create(atm_address='Ahornsgade 22, 2200 København',atm_latitude='55.693608182309916',atm_longitude="12.56030510275687")
            Atm.objects.create(atm_address='Østerågade 4A, 9000 Aalborg',atm_latitude='57.04826146997335',atm_longitude="9.920649860944506")
            Atm.objects.create(atm_address='Slotsherrensvej 103, 2720 København',atm_latitude='55.700009324491134',atm_longitude="12.475780247973942")
            Atm.objects.create(atm_address='Kronprinsensgade 1, 3, 6700 Esbjerg',atm_latitude='55.46661527660702',atm_longitude="8.445973998411711")
       
    