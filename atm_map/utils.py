import requests


def getAtmCoordinates(atm_adress):
		url = f'https://api.mapbox.com/geocoding/v5/mapbox.places/{atm_adress}.json'
		params = {'access_token': 'pk.eyJ1Ijoia2xhamRpNDQiLCJhIjoiY2wzOHE2NG05MDA4czNrbzJnbnBudzBmYSJ9.9C7zZSRoGr12Z5Eo3QpNog','limit':'1'}
		r = requests.get(url, params=params)
		longitude = None
		latitude = None
		error = None

		if(r.status_code == requests.codes.ok):
			results = r.json()
			try:
				longitude = results['features'][0]['geometry']['coordinates'][0] 
				latitude = results['features'][0]['geometry']['coordinates'][1] 
			except KeyError:
				error = 'Error, key does not exist in Dictonary, api error'
		else:
				error = 'Error, Geocoding api not available, please try again later.'
		return {'longitude': longitude,'latitude':latitude,'error':error}
