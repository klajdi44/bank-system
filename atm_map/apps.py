from django.apps import AppConfig


class AtmMapConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'atm_map'
