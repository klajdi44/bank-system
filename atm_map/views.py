from django.shortcuts import render
from .models import Atm
from .forms import Atm_form
from .utils import getAtmCoordinates

def index(request): 
    allLocations = Atm.objects.all()
    error = None
    is_staff = False
    if request.user.is_staff:
        is_staff = True
    if request.method == 'POST':
        form = Atm_form(request.POST)
        if form.is_valid():
            address = form.cleaned_data['atm_address']
            coordinates = getAtmCoordinates(address)
            latitude = coordinates['latitude']
            longitude = coordinates['longitude']
            error  = coordinates['error']
            if not error:
             address = Atm(atm_address=address,atm_latitude=latitude,atm_longitude=longitude)
             address.save()
            else:
             error = 'Failed to create Marker'
        else:
            error = 'Form is not valid'
    else:
        form = Atm_form()

    context = {
        'atm_locations':allLocations,
        'form':form,
        'error':error,
        'is_staff':is_staff
    }
    return render(request, 'atm_map/atm_map.html', context)

