from django import forms
from .models import Atm


class Atm_form(forms.ModelForm):
    class Meta:
        model = Atm
        fields = ('atm_address',) 
        widgets={
            'atm_address': forms.TextInput(attrs = {'class':'form-control','placeholder':'E.g: Prinsesse Charlottes Gade 38, 2200 København'}),
        }