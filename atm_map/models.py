from django.db import models
from .utils import getAtmCoordinates

# Create your models here.
class Atm(models.Model):
	atm_address = models.TextField()
	atm_latitude = models.FloatField(blank=True,null=True)
	atm_longitude = models.FloatField(blank=True,null=True)
	
	def __str__(self):
		return f'{self.atm_address}'

	# def save(self,*args,**kwargs):
	# 	coordinates = getAtmCoordinates(self.atm_address)
	# 	latitude = coordinates['latitude']
	# 	longitude = coordinates['longitude']
	# 	self.atm_latitude = latitude
	# 	self.atm_longitude = longitude
	# 	return super(Atm,self).save(*args,**kwargs)
