

// focus 'roomInput' when user opens the page
document.querySelector("#roomInput").focus();

// submit if the user presses the enter key
document.querySelector("#roomInput").onkeyup = function(e) {
    if (e.keyCode === 13) {  // enter key
        document.querySelector("#roomConnect").click();
    }
};

// redirect to '/room/<roomInput>/'
document.querySelector("#roomConnect").onclick = function() {
    let roomName = document.querySelector("#roomInput").value;
    window.location.pathname = `chat/${roomName}/`;
    
}

// redirect to '/room/<roomSelect>/'
document.querySelector("#roomSelect").onchange = function() {
    let roomName = document.querySelector("#roomSelect").value.split(" (")[0];
    window.location.pathname = `chat/${roomName}/`;
    
}
document.querySelector("#roomSelect").style = 'height: 250px';

document.querySelector('#room-name-submit').onclick = function (e) {
    let roomName = document.querySelector('#room-name-input').value.replaceAll(' ', '_');
    window.location.pathname = `/chat/${roomName}/`;  
};

 //predifined rooms
// let roomNamePredefined = document.querySelectorAll('.room-name-predefined');

// for (let i = 0; i < roomNamePredefined.length; i++) {
//     roomNamePredefined[i].onclick = function (e) {
//         console.log("hey")
//         window.location.pathname = '/chat/' + roomNamePredefined[i].value.replaceAll(' ', '_') + '/';
//     };
// }


