from django.shortcuts import render
from chat.models import Room
from django.contrib.auth.decorators import login_required


@login_required
def index_view(request):
    return render(request, 'index.html', {
        'rooms': Room.objects.all(),
    })


@login_required
def room_view(request, room_name):
    chat_room, created = Room.objects.get_or_create(name=room_name)
    return render(request, 'room.html', {
        'room': chat_room,
    })
