

# Course: Back-End with Python & Django
6th semester exam project
KEA Copenhagen School of Design and Technology

# Group
Jonas Bøgh, Klajdi Ajdini, Stefan-Andrei Mihutoni


# Starting the project
To setup the database and add data to the bank, run the following commands:
```
pip install -r requirements.txt
python manage.py makemigrations
python manage.py makemigrations bank_app
python manage.py migrate
python manage.py provision
python manage.py bankdata
python manage.py atm_data
```

# Chat
The chat feature uses RedisChannelLayer.
Since this layer requires Redis, run the following command to get it up and running with Docker:
```
docker run -p 6379:6379 -d redis:5
```

# Login Credentials
After running the scripts above, you can login to the app both as a staff member and a customer.

# Staff credentials
Username: johnny
Password: bankstaff

# Customer credentials
Username: django
Password: requirements

